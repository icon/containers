FROM ubuntu:22.04
LABEL maintainer="Sergey Kosukhin <sergey.kosukhin@mpimet.mpg.de>"

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get -yqq update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get -yqq install \
      --no-install-recommends \
      ca-certificates \
      cdo \
      cmake \
      file \
      g++ \
      gcc \
      gfortran \
      git \
      ksh \
      libblas-dev \
      libeccodes-dev \
      libfyaml-dev \
      libhdf5-dev \
      liblapack-dev \
      libnetcdf-dev \
      libnetcdff-dev \
      libopenmpi-dev \
      libxml2-dev \
      make \
      perl \
      python3 \
      rsync \
      ssh \
      sudo \
      vim

# Enable the oversubscription when running mpiexec from OpenMPI:
ENV OMPI_MCA_rmaps_base_oversubscribe=1

RUN useradd --home-dir /home/icon --shell /bin/bash --groups sudo icon \
 && echo '%icon ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER icon
WORKDIR /home/icon

RUN (echo 'export PS1="\[$(tput bold)\]\[$(tput setaf 2)\]\u@\[$(tput setaf 3)\]dev-gcc\[$(tput sgr0)\]:\w$ "') > ~/.bashrc
